# Open Celtic Dictionary Website

The source code for the Open Celtic Dictionary web-interface website.

## Translations

This website uses a JSON file with all the strings and text used on the website.
This allows the website to be translated into several languages.
These translations are not complete, but if you want to contribute please feel free to do so :)

![Eyy](./img/carry.png)

