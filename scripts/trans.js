
"use strict";

function translationsToDiv(results, obj){
	var div = document.createElement('div');
	div.classList.add('result');
	div.append(createTextElement('h2', capitalize(obj.word)));
	var translations = createTranslationTable(obj.results, ["en"]);
	if(translations){
		div.append(createTextElement('h3', 'Translations:'));
		div.append(translations);
	}
	else {
		div.append(createTextElement('h3', 'No translations available :('));
	}
	results.append(div);
}
