
"use strict";

function idlistToDiv(results, obj){
	
	var div = document.createElement('div');
	div.classList.add('result')
	div.append(createTextElement('h2', 'ID List'));
	div.append(createTextElement('h3',
		'Between ' + obj.min + ' and ' + obj.max));

	var table = document.createElement('table');
	var head = document.createElement('tr');
	head.append(createTextElement('th', 'ID'));
	head.append(createTextElement('th', 'Word'));
	for(var i = obj.min; i <= obj.max; ++i){
		var row = document.createElement('tr');
		row.append(createTextElement('td', i));
		var index = '' + i;
		if(index in obj.results){
			var td = document.createElement('td');
			td.append(createClickableWord(obj.results[index], i, 'td'));
			row.append(td);
		}
		else
			row.append(createTextElement('td', ''))
		table.append(row);
	}

	div.append(table);
	results.append(div);
}



