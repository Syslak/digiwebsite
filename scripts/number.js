
"use strict";

function convertNumberToDiv(results, number){
	var div = document.createElement('div');
	div.classList.add('result');

	div.append(createTextElement('h2', number.number));

	var table = document.createElement('table');

	for(var type in numberTypes)
		if(type in number)
			table.append(createRow(numberTypes[type], number[type]));

	div.append(table);
	results.append(div);
};

const numberTypes = {
	'br_masc': "Breton Masculine",
	'br_fem': "Breton Feminine"
};


