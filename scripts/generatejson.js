
var words = [];

function addWordToList(){
	var newWord = document.getElementById('newWord');
	newWord.classList.add('result');
	var newWords = newWord.value.split(' ');
	newWords.forEach(function(word){
		if(word != "")
			words.push(word.toLowerCase().trim());
	});
	newWord.value = "";

	printWordList();
}

function printWordList(){
	var results = createResultsDiv();
	var div = document.createElement('div');
	div.classList.add('result');
	div.appendChild(createTextElement('h2', 'Words'));
	words.forEach(function(x){
		div.appendChild(createTextElement('p', x));
	});

	results.appendChild(div);
	replaceResults(results);
}

async function generateJson(){
	var code = document.createElement('textarea');
	code.readOnly = true;
	var id = document.getElementById('startIndex').value;
	if(id == "")
		id = 0;

	var indexes = {
		max: -1
	};

	// To make indexes start at the correct one we need to decrement it by one
	id--;

	for(var i = 0; i < words.length; ++i){
		while(indexes.max < 0 || id.toString() in indexes.results){
			id++;
			if(id > indexes.max)
				await fetch("http://prv.cymru:13999/api/dictionary/idlist?min=" + id +
					"&max=" + id+1000)
					.then(res => res.json())
					.then(obj => {
						indexes = obj;
					})
		}
		code.innerHTML += "{\n";
		code.innerHTML += '\t"normalForm": "' + words[i] + '",\n';
		code.innerHTML += '\t"id": ' + id + '\n';
		id++;
		code.innerHTML += '}';
		if(i+1 != words.length)
			code.innerHTML += ',';
		code.innerHTML += '\n';
	}

	code.rows = code.innerHTML.split('\n').length + 5;


	// print results
	var results = createResultsDiv();
	var div = document.createElement('div');
	div.classList.add('result');
	div.appendChild(createTextElement('h2', 'JSON'));
	div.appendChild(code);
	results.appendChild(div);
	replaceResults(results);
}

function clearWordList(){
	words = [];
	printWordList();
}

